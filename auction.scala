import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import scala.concurrent.duration._
import scala.language.postfixOps

abstract class AuctionMessage
case class Offer(bid: Int, client: ActorRef) extends AuctionMessage
case class Inquire(client: ActorRef) extends AuctionMessage

abstract class AuctionReply
case class Status(asked: Int) extends AuctionReply
case object BestOffer extends AuctionReply
case class BeatenOffer(maxBid: Int) extends AuctionReply

case class AuctionConcluded(seller: ActorRef, client: ActorRef) extends AuctionReply
case object AuctionFailed extends AuctionReply
case object AuctionOver extends AuctionReply

object Shutdown

abstract class MahActor extends Actor {
	def say(whatToSay: String) {
		println("Actor: " + self.path.name + " " + whatToSay)
	}
}

class Bidder extends MahActor {
	def receive = {
		case Status(maxBid) =>
			say("Max bid: " + maxBid)
		case BeatenOffer(bid) =>
			say("Somebody outbid me: " + bid)
		case BestOffer =>
			say("Yay! My offer is the best!")
		case _ =>
			say("D'oh")
	}
}

class Seller extends MahActor {
	def receive = {
		case _ =>
			say("D'oh")
	}
}

class Auction(seller: ActorRef, minBid: Int) extends Actor {
	val bidIncrement = 10
	var maxBid = minBid - bidIncrement
	var maxBidder: ActorRef = null

	def receive = {
		case Offer(bid, client) =>
			if (bid >= maxBid + bidIncrement) {
				if (maxBid >= minBid) 
					maxBidder ! BeatenOffer(bid)
				maxBid = bid
				maxBidder = client
				client ! BestOffer
			} else {
				client ! BeatenOffer(maxBid)
			}
		case Inquire(client) =>
			client ! Status(maxBid)
		case Shutdown =>
			context.system.shutdown()
	}
}

object AuctionExample extends App {
	override def main(args: Array[String]) {
		val system = ActorSystem("DefaultSystem")
		val seller = system.actorOf(Props[Seller], name = "SellerActor")
		val auctionActor = system.actorOf(Props(new Auction(seller, 100)), name = "AuctionActor")
		
		val bidder1 = system.actorOf(Props[Bidder], name = "BidderActor1")
		val bidder2 = system.actorOf(Props[Bidder], name = "BidderActor2")
		val bidder3 = system.actorOf(Props[Bidder], name = "BidderActor3")

		auctionActor ! Inquire(bidder1)
		auctionActor ! Offer(110, bidder1)
		auctionActor ! Inquire(bidder2)
		auctionActor ! Offer(115, bidder2)
		auctionActor ! Offer(120, bidder3)

		import system.dispatcher
		system.scheduler.scheduleOnce(50 milliseconds) {
			auctionActor ! akka.actor.PoisonPill
			seller ! akka.actor.PoisonPill
			bidder1 ! akka.actor.PoisonPill
			bidder2 ! akka.actor.PoisonPill
			bidder3 ! akka.actor.PoisonPill
		}
		system.scheduler.scheduleOnce(100 milliseconds) {
			auctionActor ! Shutdown
			System.exit(0)
		}

		return
	}
}